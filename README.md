# GapTool #

Scans MP3 files for silence gaps and writes a CSV file with the results.

Installation
-----
**Requirements:**

GapTool requires the libavfilter that's a part of FFmpeg.
To check if it's installed run: `ffmpeg -version` in the terminal.
You should see output similar to this:

    ffmpeg version 3.0.2 Copyright (c) 2000-2016 the FFmpeg developers
	built with Apple LLVM version 7.0.2 (clang-700.1.81)
	configuration: --prefix=/usr/local/Cellar/ffmpeg/3.0.2 --
	version3 --enable-hardcoded-tables --enable-avresample --
	cc=clang --host-cflags= --host-ldflags= --enable-opencl --
	enable-libx264 --enable-libmp3lame --enable-libxvid --
	enable-vda
	libavutil      55. 17.103 / 55. 17.103
	libavcodec     57. 24.102 / 57. 24.102
	libavformat    57. 25.100 / 57. 25.100
	libavdevice    57.  0.101 / 57.  0.101
	libavfilter     6. 31.100 /  6. 31.100
	libavresample   3.  0.  0 /  3.  0.  0
	libswscale      4.  0.100 /  4.  0.100
	libswresample   2.  0.101 /  2.  0.101
	libpostproc    54.  0.100 / 54.  0.100
To install FFmpeg if you do not see similar output as above run:

	brew install ffmpeg

To install simply download [GapTool](https://bitbucket.org/rr_apps/gap_tool/downloads) and unzip.

Usage
-----
Open the gap_tool.rb file in a code editor and change the `File_Path` constant.
By default `FILE_PATH` points to the *mp3s* directory that contains the the test pb's.

Simply change this to the path where the pb's are that you want to scan.
	i.e. `FILE_PATH = '~/Desktop/All_PBs`
Or you can leave it as it is to scan the test files first making sure it works on your system.

Now you can run the scan
	`ruby gap_tool.rb`

GapTool will scan each file and write the results to a CSV `gaps_report.csv`
Here's an example of the output from a scan of the test files.

	1,pb001001.mp3,2
	2,pb001009.mp3,2
	3,pb001018.mp3,2
	4,pb001026.mp3,2
	5,pb001034.mp3,2
	6,pb001042.mp3,2
	7,pb001051.mp3,2
	8,pb001059.mp3,2
	9,pb001068.mp3,2
	10,pb001077.mp3,2
	11,pb001991.mp3,1
	12,pb001999.mp3,1
	13,pb002007.mp3,1
	14,pb002015.mp3,1
	15,pb002023.mp3,1
	16,pb002033.mp3,1
	17,pb002041.mp3,1
	18,pb002049.mp3,1
	19,pb002057.mp3,1
	20,pb002065.mp3,1
