require 'fileutils'
require 'pathname'

FILE_PATH = 'mp3s'

MP3_PATH = File.join(FILE_PATH, '*')
FFMPEG = `which ffmpeg`.chomp.freeze
FFMPEG_CMD = '-af silencedetect=noise=-50dB:d=1 -f null - 2>&1'

def path_expansion
  File.expand_path(MP3_PATH)
end

def dir_entries
  Dir.glob(path_expansion)
end

def list_of_files
  dir_entries - %w(. .. .DS_Store)
end

def build_command(pb)
  "#{FFMPEG} -i '#{pb}' #{FFMPEG_CMD}"
end

def run_command(cmd)
  `#{cmd}`
end

def select_from_word(res)
  res.split('help')[1]
end

def delete_brackets(res)
  select_from_word(res).delete('[').delete(']')
end

def extract_array(res)
  delete_brackets(res).split("\n")
end

def is_silence_start?(seek)
  seek.include?('silence_start')
end

def count_gaps(gaps, res)
  extract_array(res).each do |seek|
    gaps += 1 if is_silence_start?(seek)
  end
  gaps
end

def build_result(gaps, ln, pb)
  res = "#{ln},#{Pathname.new(pb).basename},#{gaps}\n"
  puts res
  res
end

def write_to_file(gaps, ln, pb)
  File.open('gaps_report.csv', 'a+') do |csv|
    csv.write(build_result(gaps, ln, pb))
  end
end

ln = 0
list_of_files.each do |pb|
  gaps = 0
  cmd = build_command(pb)
  res = run_command(cmd)
  gaps = count_gaps(gaps, res)
  ln += 1
  write_to_file(gaps, ln, pb)
end
